#include "../includes/bmp.h"
#include "../includes/padding.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define COUNT_FILES 1
#define RESERVED 0 // reserved byte 
#define OFFSET 54 
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define SIGNATURE 19778 // "BM"
#define PPM 1000
#define CLRS_USED 0
#define CLRS_IMPT 0
#define SIZE_IMG 0

t_read_state from_bmp( FILE* in, t_img* img ) 
{
    t_bmp_hdr header = {0}; // zero init
    if (fread(&header, sizeof(t_bmp_hdr), COUNT_FILES, in) != 1) return READ_INVALID_HEADER;

    if (header.bfType != SIGNATURE) return READ_INVALID_SIGNATURE;

    *img = create_img(header.biWidth, header.biHeight);
    
    uint8_t pad = padding(img->width);

    for (size_t i = 0; i < img->height; ++i)
    {
        if (fread(&(img->data[(img->height - i - 1) * img->width]), sizeof(t_pel), img->width, in) != img->width) 
        {
            return READ_INVALID_BITS;
        }
        
    // перемещащем указатель из текущей позиции в начало, откуда мы начали читать
    if (fseek(in, pad, SEEK_CUR) != 0) return READ_INVALID_BITS;
    }

    return READ_OK;
}

t_write_state to_bmp( FILE* out, t_img const* img ) 
{
    const size_t pel_data_offset = sizeof(t_bmp_hdr);
    t_bmp_hdr header = 
    {
        .bfType = SIGNATURE,
        .bfileSize = pel_data_offset + img->width*img->height*sizeof(t_pel),
        .bfReserved = RESERVED,
        .bOffBits = pel_data_offset,
        .biSize = SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = PLANES,
        .biBitCount = BIT_COUNT,
        .biCompression = COMPRESSION,
        .biSizeImage = SIZE_IMG,
        .biXPelsPerMeter = PPM,
        .biYPelsPerMeter = PPM,
        .biClrUsed = CLRS_USED,
        .biClrImportant = CLRS_IMPT,
    };


    if (fwrite(&header, sizeof(t_bmp_hdr), COUNT_FILES, out) != 1) return WRITE_ERROR;

    uint8_t pad = padding(img->width);
    const uint8_t junk_bytes[] = {0, 0, 0}; // if width % 4 != 0, then added junk_bytes

    for(size_t i = 0; i < img->height; ++i) 
    {
        if (fwrite(&(img->data[(img->height - i - 1) * img->width]), sizeof(t_pel), img->width, out) != img->width)

        {
            return WRITE_ERROR;
        }

        if (fwrite(junk_bytes, sizeof(uint8_t), pad, out) != pad) 
        {
            return WRITE_ERROR;
        }

    }
    
    return WRITE_OK;
}

