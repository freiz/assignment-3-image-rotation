#include "../includes/image.h"

#include  <stdint.h>
#include <stdio.h>

t_img rotate(const t_img* input_image) 
{

    t_img rotate_img = create_img(input_image->height, input_image->width);

    for (size_t i = 0; i < input_image->height; ++i)
    {
        for (size_t j = 0; j < input_image->width; ++j)
        {
            rotate_img.data[i + rotate_img.width * (input_image->width - j - 1)] = input_image->data[j + i * input_image->width];
        }
    }
    
    return rotate_img;
}
