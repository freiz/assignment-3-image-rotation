#include "../includes/image.h"

#include  <stdint.h>
#include <stdio.h>



t_img create_img(uint64_t width, uint64_t height)
{
    return (t_img) 
    {
        .width = width,
        .height = height,
        .data = malloc(sizeof(t_pel) * width * height),
    };
}

void destroy_img(t_img img) 
{
    free(img.data);
}

