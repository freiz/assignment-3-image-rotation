#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

uint8_t padding(uint32_t width) 
{
    if (width % 4 == 0) return 0;
    else return (4 - (width * 3) % 4);
}
