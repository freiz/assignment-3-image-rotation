#include "../includes/bmp.h"
#include "../includes/padding.h"
#include "../includes/rotate.h"

#include  <stdint.h>
#include <stdio.h>

int main(int argc, char** argv)
{
    if (argc != 3) 
    {
        puts("Require args may be only 3\n");
        return EXIT_FAILURE;
    }

    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) return EXIT_FAILURE;

    t_img img = {0};
    if (from_bmp(in, &img) != READ_OK) return EXIT_FAILURE;

    if (fclose(in) != 0) return EXIT_FAILURE;

    t_img rotate_img = rotate(&img);
    destroy_img(img);

    FILE *out = fopen(argv[2], "wb");
    if (out == NULL) return EXIT_FAILURE;

    if (to_bmp(out, &rotate_img) != READ_OK) return EXIT_FAILURE;

    if (fclose(out) != 0) return EXIT_FAILURE;

    destroy_img(rotate_img);

    return EXIT_SUCCESS;
}
