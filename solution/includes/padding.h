#ifndef PADDING_H
#define PADDING_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/* Если ширина изображения в байтах кратна четырём, 
то строчки идут одна за 
другой без пропусков.
Если ширина не кратна четырём, 
то она дополняется мусорными байтами до ближайшего числа, 
кратного четырём.
Эти байты называются padding. */
uint8_t padding(const uint32_t width);

#endif
