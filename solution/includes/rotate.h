#ifndef ROTATE_H
#define ROTATE_H

#include "../includes/image.h"

t_img rotate(const t_img* input_image);

#endif
