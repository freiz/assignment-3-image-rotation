#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct pixel t_pel;


struct image
{
    uint64_t width, height;
    t_pel* data;
};

typedef struct image t_img;

struct pixel
{
    uint8_t r, g, b;
} __attribute__((packed));

t_img create_img(uint64_t width, uint64_t height);

void destroy_img(t_img img);

#endif
