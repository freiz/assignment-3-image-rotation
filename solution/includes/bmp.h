#ifndef BMP_H
#define BMP_H

#include "../includes/image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  /* коды других ошибок  */
  };

/*  serializer   */
enum write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

typedef enum read_status t_read_state;

typedef enum write_status t_write_state;

typedef struct bmp_header t_bmp_hdr;

t_read_state from_bmp( FILE* in, struct image* img );


t_write_state to_bmp( FILE* out, struct image const* img );

#endif
